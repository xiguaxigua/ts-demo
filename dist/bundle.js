/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/checkbox-tree/lib/index.js":
/*!*************************************************!*\
  !*** ./node_modules/checkbox-tree/lib/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function __$styleInject(css, returnValue) {
  if (typeof document === 'undefined') {
    return returnValue;
  }
  css = css || '';
  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';
  head.appendChild(style);
  
  if (style.styleSheet){
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
  return returnValue;
}

function extend(base, addon) {
  Object.keys(addon).forEach(function (key) {
    base[key] = addon[key];
  });
  return base;
}

function noop() {}

function createElement(tagName, attrs) {
  var node = document.createElement(tagName);
  for (var attr in attrs) {
    if (attrs.hasOwnProperty(attr)) {
      node[attr] = attrs[attr];
    }
  }
  return node;
}

__$styleInject(".checkbox-tree .tree-node-child {\n  margin-left: 15px;\n  display: none;\n}\n\n.checkbox-tree .tree-node-child.expened {\n  display: block;\n}\n\n.checkbox-tree .tree-icon {\n  display: inline-block;\n  cursor: pointer;\n  width: 0;\n  height: 0;\n  margin-top: 5px;\n  margin-right: 3px;\n  border: 5px solid transparent;\n  border-right-width: 0;\n  border-left-color: #333;\n  border-left-width: 7px;\n}\n\n.checkbox-tree .tree-icon.expened {\n  border: 5px solid transparent;\n  border-bottom-width: 0;\n  border-top-color: #333;\n  border-top-width: 7px;\n  margin-top: 7px;\n  margin-right: 0;\n}\n\n.checkbox-tree .tree-icon.empty {\n  border-color: transparent;\n}\n", undefined);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var defaultLevel = {
  children: 'children',
  checkedProperty: 'flag',
  name: 'name',
  id: 'id',
  parentId: 'parent_id',
  expened: false
};

var checkboxTree = function () {
  function checkboxTree(node, _ref) {
    var _ref$data = _ref.data,
        data = _ref$data === undefined ? [] : _ref$data,
        _ref$level = _ref.level,
        level = _ref$level === undefined ? {} : _ref$level,
        _ref$changeHandler = _ref.changeHandler,
        changeHandler = _ref$changeHandler === undefined ? noop : _ref$changeHandler;

    _classCallCheck(this, checkboxTree);

    if (!node) return;
    var settings = extend(defaultLevel, level);
    this.node = node;
    this.data = data;
    this.changeHandler = changeHandler || noop;
    this.childrenSign = settings.children;
    this.checkedProperty = settings.checkedProperty;
    this.nameSign = settings.name;
    this.idSign = settings.id;
    this.parentIdSign = settings.parentId;
    this.expened = settings.expened;
    this.dataMap = {};
    this.selectedData = [];
    this.init();
  }

  _createClass(checkboxTree, [{
    key: 'init',
    value: function init() {
      var checkboxTree = createElement('div', { className: 'checkbox-tree' });

      checkboxTree.appendChild(this.getTreeDom(this.data));
      this.node.appendChild(checkboxTree);
      checkboxTree.addEventListener('click', this.clickHandler.bind(this));
    }
  }, {
    key: 'getTreeDom',
    value: function getTreeDom(nodeData) {
      var _this = this;

      var fragment = document.createDocumentFragment();

      nodeData.forEach(function (value) {
        var treeNode = createElement('div', { className: 'tree-node' });

        _this.dataMap[value[_this.idSign]] = value;

        if (value[_this.childrenSign] && value[_this.childrenSign].length) {
          _this.checkAndInterminate = [false, false];

          var status = _this.getStatus(value[_this.childrenSign]);
          var contentProp = {
            text: value[_this.nameSign],
            id: value[_this.idSign],
            checked: status[0],
            fill: true,
            indeterminate: status[1]
          };

          treeNode.appendChild(_this.getTreeNodeContent(contentProp));
          treeNode.appendChild(_this.getTreeNodeChild(_this.getTreeDom(value[_this.childrenSign])));
        } else {
          var _contentProp = {
            text: value[_this.nameSign],
            id: value[_this.idSign],
            checked: value[_this.checkedProperty],
            fill: false,
            indeterminate: false
          };

          treeNode.appendChild(_this.getTreeNodeContent(_contentProp));
        }

        fragment.appendChild(treeNode);
      }, this);

      return fragment;
    }
  }, {
    key: 'getTreeNodeContent',
    value: function getTreeNodeContent(_ref2) {
      var text = _ref2.text,
          id = _ref2.id,
          checked = _ref2.checked,
          fill = _ref2.fill,
          indeterminate = _ref2.indeterminate;

      var emptySign = fill ? '' : 'empty';
      var expened = this.expened ? 'expened' : '';
      var node = createElement('div', { className: 'tree-node-content' });

      node.appendChild(createElement('span', { className: 'tree-icon ' + emptySign + ' ' + expened }));
      node.appendChild(createElement('input', { type: 'checkbox', name: id, checked: checked, indeterminate: indeterminate }));
      node.appendChild(createElement('span', { className: 'tree-text', innerHTML: text }));

      return node;
    }
  }, {
    key: 'getStatus',
    value: function getStatus(children) {
      this.getCheckAndInterminate(children);
      var result = [];

      if (this.checkAndInterminate[0] && this.checkAndInterminate[1]) {
        result = [false, true];
      } else if (this.checkAndInterminate[0] && !this.checkAndInterminate[1]) {
        result = [true, false];
      } else {
        result = [false, false];
      }

      return result;
    }
  }, {
    key: 'getCheckAndInterminate',
    value: function getCheckAndInterminate(children) {
      var _this2 = this;

      children.forEach(function (child) {
        if (child[_this2.childrenSign] && child[_this2.childrenSign].length) {
          _this2.getCheckAndInterminate(child[_this2.childrenSign]);
        } else {
          if (child[_this2.checkedProperty]) {
            _this2.checkAndInterminate[0] = true;
          } else {
            _this2.checkAndInterminate[1] = true;
          }
        }
      }, this);
    }
  }, {
    key: 'getTreeNodeChild',
    value: function getTreeNodeChild(content) {
      var expened = this.expened ? 'expened' : '';
      var node = createElement('div', { className: 'tree-node-child ' + expened });

      node.appendChild(content);

      return node;
    }
  }, {
    key: 'clickHandler',
    value: function clickHandler(_ref3) {
      var target = _ref3.target;

      if (target.tagName.toLowerCase() === 'span') {
        var clickChildrenDom = target.parentNode.nextSibling;

        if (!clickChildrenDom) return;
        if (clickChildrenDom.classList.contains('expened')) {
          clickChildrenDom.classList.remove('expened');
          target.classList.remove('expened');
        } else {
          clickChildrenDom.classList.add('expened');
          target.classList.add('expened');
        }
      } else if (target.tagName.toLowerCase() === 'input') {
        this.checkboxClickHandler(target);
        this.changeHandler();
      }
    }
  }, {
    key: 'getSelectedNodes',
    value: function getSelectedNodes(leafNodeOnly, exceptIndeterminate) {
      var _this3 = this;

      var input = this.node.querySelectorAll('input');
      var selectedData = [];

      input.forEach(function (item) {
        if (leafNodeOnly) {
          if (item.checked && !item.parentNode.nextSibling) {
            selectedData.push(_this3.dataMap[item.name]);
          }
        } else {
          if (exceptIndeterminate) {
            if (item.checked && !item.indeterminate) {
              selectedData.push(_this3.dataMap[item.name]);
            }
          } else {
            if (item.checked || item.indeterminate) {
              selectedData.push(_this3.dataMap[item.name]);
            }
          }
        }
      }, this);

      return selectedData;
    }
  }, {
    key: 'changeData',
    value: function changeData(data) {
      this.data = data;
      this.destory();
      this.init();
    }
  }, {
    key: 'destory',
    value: function destory() {
      this.node.innerHTML = null;
      this.node.removeEventListener('click', this.clickHandler.bind(this));
    }
  }, {
    key: 'checkboxClickHandler',
    value: function checkboxClickHandler(target) {
      var _this4 = this;

      var clickChildrenDom = target.parentNode.nextSibling;
      var parentDom = target.parentNode.parentNode.parentNode;

      if (target.checked === true) {
        // 自身被选中
        this.dataMap[target.name][this.checkedProperty] = true;

        if (clickChildrenDom) {
          // 子元素全部选中
          clickChildrenDom.querySelectorAll('input').forEach(function (value) {
            value.checked = true;
            _this4.dataMap[value.name][_this4.checkedProperty] = true;
          }, this);
        }
        // 处理父元素逻辑
        this.parentChangeHandler(parentDom, true, false);
      } else {
        // 自身被移除
        this.dataMap[target.name][this.checkedProperty] = false;

        if (clickChildrenDom) {
          // 子元素全部移除
          clickChildrenDom.querySelectorAll('input').forEach(function (value) {
            value.indeterminate = false;
            value.checked = false;
            _this4.dataMap[value.name][_this4.checkedProperty] = false;
          }, this);
        }
        // 处理父元素逻辑
        this.parentChangeHandler(parentDom, false, false);
      }
    }
  }, {
    key: 'parentChangeHandler',
    value: function parentChangeHandler(parentDom, checked, childIndeterminate) {
      if (!parentDom.previousSibling) return;

      var parentInput = parentDom.previousSibling.querySelector('input');
      // 默认无半选状态
      var indeterminate = false;
      var parentDomChildren = Array.prototype.slice.call(parentDom.children);

      if (checked) {
        // 元素自身被选中， 检查兄弟节点是否全选
        parentDomChildren.some(function (child) {
          var childInput = child.querySelectorAll('input');
          var inputArray = Array.prototype.slice.call(childInput);
          var indStatus = inputArray.some(function (value) {
            return !value.checked;
          });
          if (indStatus) {
            indeterminate = true;
            return true;
          }
        });
        // 赋值状态
        parentInput.checked = true;
        parentInput.indeterminate = childIndeterminate || indeterminate;

        if (!parentInput.indeterminate) {
          this.dataMap[parentInput.name][this.checkedProperty] = true;
        }
      } else {
        // 元素自身被移除， 检查兄弟节点是否全选
        parentDomChildren.some(function (child) {
          var sibingInput = child.children[0].querySelectorAll('input')[0];
          if (sibingInput.checked || sibingInput.indeterminate) {
            // 如果有被选中的，则半选状态并跳出循环
            indeterminate = true;
            return true;
          }
        });
        // 赋值状态
        parentInput.checked = indeterminate;
        parentInput.indeterminate = childIndeterminate || indeterminate;

        this.dataMap[parentInput.name][this.checkedProperty] = indeterminate;
      }

      if (parentDom.parentNode.parentNode.classList.contains('tree-node-child')) {
        // 递归父节点
        this.parentChangeHandler(parentDom.parentNode.parentNode, checked, indeterminate);
      }
    }
  }]);

  return checkboxTree;
}();

module.exports = checkboxTree;


/***/ }),

/***/ "./src/declatation.ts":
/*!****************************!*\
  !*** ./src/declatation.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// import { a } from './test-module/index.js'
// console.log(a)
const checkbox_tree_1 = __webpack_require__(/*! checkbox-tree */ "./node_modules/checkbox-tree/lib/index.js");
// import { noop } from 'lodash'
// console.log(tree)
console.log(checkbox_tree_1.default);


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// import './simple'
// import './class'
// import './function'
// import './generic'
// import './enums'
// import './type'
// import './intersection'
// import './symbols'
// import './namespace'
// import './decorators'
__webpack_require__(/*! ./declatation */ "./src/declatation.ts");


/***/ })

/******/ });