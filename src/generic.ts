function identity (arg: any): any {
  return arg
}

function identity1<T> (arg: T): T {
  return arg
}

let output = identity1<string>('1')
let output1 = identity1('1')

function loggingIdentity<T> (arg: T[]): T[] {
  console.log(arg.length)
  return arg
}

function loggingIdentity1<T> (arg: Array<T>): Array<T> {
  console.log(arg.length)
  return arg
}

interface GenericIdentityFn {
  <T> (arg: T): T
}

let myIdentity: GenericIdentityFn = identity

interface GenericIdentityFn1<T> {
  (arg: T): T
}

let myIdentity1: GenericIdentityFn1<number> = identity

class GenericNumber<T> {
  zeroValue: T
  add: (x: T, y: T) => T
}

let myGenericNumber = new GenericNumber<number>()
myGenericNumber.zeroValue = 1
myGenericNumber.add = function (x, y) { return x + y }

interface Lengthwise {
  length: number
}

function loggingIdentity2<T extends Lengthwise> (arg: T): T {
  console.log(arg.length)
  return arg
}

loggingIdentity2({ value: 1, length: 1 })
