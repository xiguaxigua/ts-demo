class Greeter {
  greeting: string
  constructor (message: string) {
    this.greeting = message
  }
  greet () {
    return `hello ${this.greeting}`
  }
}

let greeter = new Greeter('2')

class Ani {
  protected name: string
  constructor (theName: string) { this.name = theName }
  move (dis: number = 0) { console.log(`${dis} - ${this.name}`) }
}

class Sna extends Ani {
  constructor (theName: string) { super(theName) }
  move (dis = 1) { console.log(1); super.move(dis) }
}

class Hor extends Ani {
  constructor (theName: string) { super(theName); console.log(this.name) }
  move (dis = 2) { console.log(1); super.move(dis) }
}

class Oct {
  readonly name: string
  constructor (theName: string) {
    this.name = theName
  }
}

let bob = new Oct('123')
// bob.name = 1
