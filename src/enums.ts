enum Direction {
  Up = 1,
  Down,
  Left,
  Right
}

enum Response1 {
  No = 0,
  Yes = 1
}

function response1 (rec: string, msg: Response1): void {

}
response1('1', Response1.Yes)

enum Direction1 {
  Up = 'Up',
  Down = 'Down'
}

enum ShapeKind {
  Circle,
  Square
}

interface Circle {
  kind: ShapeKind.Circle
  radius: number
}

interface Square {
  kind: ShapeKind.Square
  sideLength: number
}

let c: Circle = {
  kind: ShapeKind.Circle,
  radius: 100
}

