function extend<T, U> (first: T, section: U): T & U {
  let result = <T & U>{}
  for (let id in first) {
    (<any>result)[id] = (<any>first)[id]
  }
  return result
}

class Person {
  constructor (public name: string) {}
}

interface Loggable {
  log(): void
}

class ConsoleLogger implements Loggable {
  log () {}
}
var jim = extend(new Person('jim'), new ConsoleLogger())

function padLeft (v: string | number) {
  console.log(v)
}
padLeft(1)

interface Bird {
  fly(): void
  layEggs(): void
}

interface Fish {
  swim(): void
  layEggs(): void
}

// function getSmallPet (): Fish | Bird {
//   return { fly () {} }
// }

function isFish (pet: Fish | Bird): pet is Fish {
  return (<Fish>pet).swim !== undefined
}

type Easing = 'a' | 'b' | 'c'
class UI {
  animate (dx: number, dy: number, easing: Easing) {}
}
let btn = new UI()
btn.animate(0, 0, 'a')
