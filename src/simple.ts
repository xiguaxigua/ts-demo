let isDone: boolean = false
let decLiteral: number = 6
let name1: string = 'bob'
let list: number[] = [1, 2, 3]
let list1: Array<number> = [1, 2, 3]
let x: [string, number] = ['1', 2]

enum Color { Red, Green, Blue }
let c1: Color = Color.Green

enum Color1 { Red = 1, Green = 2, Blue = 4 }
let d: Color1 = Color1.Red

let notSure: any = 4
let list2: any[] = [1, '2', true]

function warnuser (): void {
  console.warn(1)
}
let unusable: void = undefined
let unusable1: undefined = undefined

function error1 (message: string): never {
  throw new Error(message)
}

function printLabel (labelledObj: { label: string }) {
  console.log(labelledObj.label)
}

let myObj = { size: 10, label: 'label' }
printLabel(myObj)

interface LabelledValue {
  label: string,
  size?: number
}

function printLabel1 (labelledObj: LabelledValue) {
  console.log(labelledObj.label)
}

printLabel1(myObj)

interface Point {
  readonly x: number;
  readonly y: number;
}

let p1: Point = { x: 1, y: 2 }

interface SearchFunc {
  (source: string, subString: string): boolean
}

let mySearch: SearchFunc = function (a, b) {
  let result = a.search(b)
  return result > 1
}

interface ClockInterface {
  currTime: Date
  setTime(d: Date): void
}

class Clock implements ClockInterface {
  currTime: Date
  setTime(d: Date) {
    this.currTime = d
  }
  constructor (h: number, m: number) {
    console.log(this.currTime)
  }
}

interface Shape {
  color: string
}

interface Shape1 {
  width: string
}

interface Square extends Shape, Shape1 {
  sideLength: number
}

let square = <Square>{}
square.color = '1'


interface Counter {
  (start: number): string
  interval: number
  reset (): void
}

function getCounter (): Counter {
  let counter = <Counter>function (start: number) {}
  counter.interval = 123
  counter.reset = function () {}
  return counter
}
